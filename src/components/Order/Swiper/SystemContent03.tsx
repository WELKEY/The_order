import React from "react";
import SystemBanner03 from "../../../assets/images/Systembanner/SystemBanner03.png";
import "../../../assets/css/Order.css";
import { useMediaQuery } from "react-responsive";

function SystemContent03() {
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 900px)" });

  return (
    <div id="Content_Wrap">
      <div className="table_mockup" style={{ paddingTop: isTabletOrMobile ? 10 : 20 }}>
        <img src={SystemBanner03} alt="" />
      </div>
      <p>따로 직원을 부르지 않아도 앉은 자리에서 요청 가능</p>
    </div>
  );
}

export default SystemContent03;
